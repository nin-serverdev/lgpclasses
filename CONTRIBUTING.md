# Contributing
Pull requests are welcome!

##### The following procedures regarding source file headers shall be followed by all contributors whose changes are committed to the official project source repository:
- When a new source file is created, the header shall be prepended to the file before the file is committed to the source repository. The original file author shall include his/her name in the copyright statement along with the year the file was created.
- Additional contributors shall add their names and respective years of contribution in the copyright statement if they make changes to the file in the source repository. Years can be indicated as ranges, or separated by comas.
- While not strictly required, consider including an e-mail address in addition to the contributor name.